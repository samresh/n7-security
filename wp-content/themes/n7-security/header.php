<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css" />
	
	
	
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="trigger"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/trigger.png"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="logo"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="n7"></a>
				</div>
			</div>
			<div class="col-md-5">
				<div class="header-right">
					<ul>
						<li><span class="call">Let’s talk <a href="tel:+61 1300 671 300">+61 1300 671 300</a></span>
						</li>
						<li><span class="qot"><a href="javascript:;">Request a qoute</a>
						</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>







